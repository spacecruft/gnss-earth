# Spacecruft GNSS Earth
Spacecruft GNSS Earth is a series of web applications that render
GNSS satellites on a globe of Earth (here).


# Websites
Websites available for viewing.

![spacecruft-beidou](pics/beidou.png)
* https://beidou.spacecruft.org/

![spacecruft-galileo](pics/galileo.png)
* https://galileo.spacecruft.org/

![spacecruft-gps](pics/gps.png)
* https://gps.spacecruft.org/

![spacecruft-glonass](pics/glonass.png)
* https://glonass.spacecruft.org/

![spacecruft-gnss](pics/gnss.png)
* https://gnss.spacecruft.org/


# Scripts
The following scripts are available for running websites:

* `gnss-earth.py` --- All satellites
* `beidou-earth.py` --- Beidou (China) satellites
* `galileo-earth.py` ---  Galileo (Europe) satellites
* `glonass-earth.py` ---  GLONASS (Russia) satellites
* `gps-earth.py` --- GPS (USA) satellites


# Installation
To install on your own machine:

```
sudo apt update
sudo apt install python3-pip

git clone https://spacecruft.org/spacecruft/gnss-earth
cd gnss-earth/
# Install python setup to your taste...
# Update pip...
pip3 install --user --upgrade pip
pip3 install --user --upgrade -r requirements.txt
```

Need to get `cesium.key` from Cesium ION.

Run the app, such as: `python3 beidou-earth.py`

Then you can view it in a web browser. Each script has it's own port.
URL will be like http://127.0.0.1:8051

If you want a more semi-permanent install with a web proxy that starts on
boot, see you can do below:

There are sample Apache configs in the `apache/` directory.

There are systemd files with really bad PATHs in the `systemd/` directory.
Copy them here: `/etc/systemd/system/`

Run: `sudo systemctl daemon-reload`

Then run `systemctl start spacecruft-beidou` or whatever service
you want to start.

You can see what ports it is listening on (from `net-tools` package) ala:
`netstat -pant | grep py`


# Upstream Dependencies
Uses:

* Python
* Dash
* https://github.com/cassova/satellite-czml
* https://celestrak.com/NORAD/elements/


# License / Copyright
Copyright (C) 2022 Jeff Moe.

License: AGPLv3 or any later version.
