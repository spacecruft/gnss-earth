#!/usr/bin/python3

import random
import urllib.request
import dash
from dash import html
from dash import dcc
from dash.dependencies import Input, Output
from datetime import datetime, timedelta
from satellite_czml import satellite_czml
from satellite_czml import satellite

# GNSS
url = 'https://celestrak.com/NORAD/elements/gnss.txt'
# GPS Operational
#url = 'https://celestrak.com/NORAD/elements/gps-ops.txt'
# Galileo
#url = 'https://celestrak.com/NORAD/elements/galileo.txt'
# GLONASS Operational
#url = 'https://celestrak.com/NORAD/elements/glo-ops.txt'
# Beidou
#url = 'https://celestrak.com/NORAD/elements/beidou.txt'

apikey = open('cesium.key').read().strip()

tles = [l.decode("utf-8").strip() for l in urllib.request.urlopen(url).readlines()]
tle_list = [[tles[i],tles[i+1],tles[i+2]] for i,_ in enumerate(tles) if i%3==0]

multiple_czml = satellite_czml(tle_list=tle_list).get_czml()

# DASH
external_css = ['https://cesium.com/downloads/cesiumjs/releases/1.76/Build/Cesium/Widgets/widgets.css']
external_scripts = [{'src':'https://cesium.com/downloads/cesiumjs/releases/1.76/Build/Cesium/Cesium.js'}]

app = dash.Dash(__name__,
                title='Spacecruft GNSS Earth',
                external_scripts=external_scripts,
                external_stylesheets=external_css)

app.layout = html.Div(children=[
    html.Div(id='cesiumContainer'),
    #html.Div(id='czml', style={'display': 'none'}, children=single_czml),
    html.Div(id='czml', style={'display': 'none'}, children=multiple_czml),
    html.Div(id='apikey', style={'display': 'none'}, children=apikey),
])

app.clientside_callback(
    '''
function(id, czml, apikey) {
    // Create the Cesium Viewer
    if (!window.viewer) {
        Cesium.Ion.defaultAccessToken = apikey;
        window.viewer = new Cesium.Viewer(id,{
            shouldAnimate: false,
        });
        window.viewer.scene.globe.enableLighting = true;
    }

    // Update the Cesium Viewer
    if (czml) {
        window.viewer.dataSources.removeAll();
        czmlJson = JSON.parse(czml);
        window.viewer.dataSources.add(
            Cesium.CzmlDataSource.load(czmlJson)
        );
    }
        return true;
    }
    ''',
    Output('cesiumContainer', 'data-done'),
    Input('cesiumContainer', 'id'),
    Input('czml', 'children'),
    Input('apikey', 'children')
)

if __name__ == '__main__':
    app.run_server(debug=False, use_reloader=False)

